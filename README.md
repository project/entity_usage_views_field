Entity Usage Views Field

Author/maintainer: [Daniel Phin](http://danielph.in)

# Dependencies

  - Views
  - [Entity Usage](https://www.drupal.org/project/entity_usage)

# License

This program is free software; you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
Street, Fifth Floor, Boston, MA 02110-1301 USA.

# Usage

 1. Enable the module.
 2. Ensure appropriate *target* entity types are configured in Entity Usage
    configuration form.
 3. Navigate to a view displaying a list of entities, of the **same entity
    type** as configured in Entity Usage.
 4. Add a new views field, located the field named _Entity usage count_.
 
## Extra feature: entity usage as modal. 
 
A useful enhancement to this field is converting the usage count text to a 
hyperlink, when clicked the Entity Usage page for the entity target type opens
in a modal dialog.

Note: The _Enabled local tasks_ option in Entity Usage configuration form for
the target entity type must be enabled.

 1. Create a new field, which renders out a URL for the usage page for your
    entity type. E.g, for node: '/node/{{ id }}/usage'. Reorder so this new
    field is before the _Entity usage count_ field.
 2. Edit the _Entity usage count_ field added per "Usage" section above.
 3. Check the _Output this field as a custom link_ checkbox found in 
    "Rewrite results" section.
 4. In the _Link path_ field, insert the token for the usage page URL.
 5. Apply field settings.

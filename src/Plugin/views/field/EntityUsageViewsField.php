<?php

declare(strict_types = 1);

namespace Drupal\entity_usage_views_field\Plugin\views\field;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity usage count.
 *
 * Shows a count of the number of usages of this entity by the default revision
 * of other entities.
 *
 * Unfortunately because we need to be revision aware, queries cannot be added
 * to the primary views query to join by revision. This means this field is not
 * sortable.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField(\Drupal\entity_usage_views_field\Plugin\views\field\EntityUsageViewsField::PLUGIN_ID)
 * @property \Drupal\views\Plugin\views\query\Sql query
 */
class EntityUsageViewsField extends NumericField {

  /**
   * The plugin ID.
   */
  public const PLUGIN_ID = 'entity_usage_views_field';

  /**
   * Constructs a new EntityUsageViewsField.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   Active database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    mixed $plugin_definition,
    protected Connection $database,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $values, $field = NULL) {
    // Get the alias of the additional field added in the views definition.
    $idAlias = $this->aliases['id'];

    $query = $this->database->select('entity_usage', 'eu');
    $query->fields('eu', [
      'source_type',
      'source_id',
      'source_id_string',
      'source_vid',
    ]);
    $query->condition('eu.target_id', $values->{$idAlias});
    $query->condition('eu.target_type', $this->configuration['entity_type']);

    $entityIdsGrouped = [];
    $results = $query->execute();
    foreach ($results as $row) {
      $entityIdsGrouped[$row->source_type][$row->source_id][] = $row->source_vid;
    }

    $usageCount = 0;
    foreach ($entityIdsGrouped as $entityType => $ids) {
      $entityStorage = $this->entityTypeManager->getStorage($entityType);
      // Load the default revision of all entities.
      $entities = $entityStorage->loadMultiple(array_keys($ids));
      foreach ($entities as $entity) {
        if (!$entity->getEntityType()->isRevisionable() && $entity instanceof RevisionableInterface) {
          // Count the whole entity once if its not revisionable.
          $usageCount++;
          continue;
        }

        $revisionIds = $ids[$entity->id()];
        // If the default revision is found in any of the usage revision ids,
        // then count the usage.
        if (in_array($entity->getRevisionId(), $revisionIds)) {
          $usageCount++;
        }
      }
    }

    return $usageCount;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Don't call parent and add additional fields only since this is a virtual
    // field.
    $this->ensureMyTable();
    $this->addAdditionalFields();
  }

  /**
   * {@inheritdoc}
   */
  protected function renderAsLink($alter, $text, $tokens) {
    // See note in parent " Allow the addition of arbitrary attributes to...".
    $alter['link_attributes'] = [
      'class' => 'use-ajax',
      'data-dialog-type' => 'modal',
      'data-dialog-options' => Json::encode([
        'width' => 700,
      ]),
    ];
    return parent::renderAsLink($alter, $text, $tokens);
  }

}

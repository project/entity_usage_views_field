<?php

declare(strict_types=1);

namespace Drupal\entity_usage_views_field;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;

/**
 * Invalidates views data when entity usage settings change.
 */
final class EuvfCacheTagInvalidator implements CacheTagsInvalidatorInterface {

  /**
   * {@inheritdoc}
   */
  public function invalidateTags(array $tags): void {
    if (in_array('config:entity_usage.settings', $tags, TRUE)) {
      // Tag as found in \Drupal\views\ViewsData, forces rebuild of
      // entity_usage_views_field_views_data_alter().
      Cache::invalidateTags(['views_data']);
    }
  }

}

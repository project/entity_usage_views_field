<?php

declare(strict_types = 1);

namespace Drupal\Tests\entity_usage_views_field\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\filter\Entity\FilterFormat;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\Tests\views\Kernel\ViewsKernelTestBase;
use Drupal\views\Entity\View;
use Drupal\views\Tests\ViewTestData;

/**
 * Tests the results of 'entity_usage_views_field' field plugin.
 *
 * @coversDefaultClass \Drupal\entity_usage_views_field\Plugin\views\field\EntityUsageViewsField
 *
 * @group entity_usage_views_field
 */
final class EuvfViewsFieldTest extends ViewsKernelTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'entity_usage_views_field_test',
    'entity_usage_views_field',
    'entity_test',
    'entity_usage',
    'filter',
    'field',
    'text',
  ];

  /**
   * {@inheritdoc}
   */
  public static $testViews = [
    'entity_test_usage_test_view',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE): void {
    parent::setUp(FALSE);
    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('user');
    $this->setUpCurrentUser();
    $this->installSchema('entity_usage', ['entity_usage']);
    ViewTestData::createTestViews(get_class($this), ['entity_usage_views_field_test']);
    $fieldStorage = FieldStorageConfig::create([
      'type' => 'text_long',
      'entity_type' => 'entity_test',
      'field_name' => 'foo',
    ]);
    $fieldStorage->save();
    FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'entity_test',
      'label' => 'Foo',
    ])->save();

    \Drupal::configFactory()->getEditable('entity_usage.settings')
      ->set('track_enabled_source_entity_types', ['entity_test'])
      ->set('track_enabled_target_entity_types', ['entity_test'])
      ->set('track_enabled_plugins', ['html_link'])
      ->save();

    FilterFormat::create([
      'format' => 'full_html',
      'name' => 'Full HTML',
    ])->save();
  }

  /**
   * Tests field.
   */
  public function testViewField(): void {
    $entity1 = EntityTest::create();
    $entity1->setName('Test entity 1')->save();
    $entity2 = EntityTest::create();
    $entity2->setName('Test entity 2');
    $entity2->foo = [
      'format' => 'full_html',
      'value' => '<a href="/entity_test/' . $entity1->id() . '">Link to test entity 1</a>',
    ];
    $entity2->save();

    /** @var \Drupal\entity_usage\EntityUsageInterface $entityUsage */
    $entityUsage = \Drupal::service('entity_usage.usage');
    $usage = $entityUsage->listSources($entity1);
    $this->assertEquals([
      $entity2->getEntityTypeId() => [
        $entity2->id() => [
          [
            'source_langcode' => 'en',
            'source_vid' => '0',
            'method' => 'html_link',
            'field_name' => 'foo',
            'count' => '1',
          ],
        ],
      ],
    ], $usage);
    $usage = $entityUsage->listSources($entity2);
    $this->assertEquals([], $usage);

    $view = View::load('entity_test_usage_test_view');
    /** @var \Drupal\views\ViewExecutable $executable */
    $executable = $view->getExecutable();
    $this->executeView($executable);

    $this->assertCount(2, $executable->result);
    $expectedResult = [
      ['name' => 'Test entity 1'],
      ['name' => 'Test entity 2'],
    ];
    $this->assertIdenticalResultset($executable, $expectedResult, [
      'name' => 'name',
    ]);
    $assertRendered = [
      ['entity_usage_views_field' => '1'],
      ['entity_usage_views_field' => '0'],
    ];
    $result = $executable->result;
    $this->assertEquals(array_keys($assertRendered), array_keys($result));
    foreach ($assertRendered as $rowIndex => $assertRender) {
      $rendered = $executable->field['entity_usage_views_field']->advancedRender($result[$rowIndex]);
      $this->assertEquals($assertRender['entity_usage_views_field'], $rendered);
    }
  }

}
